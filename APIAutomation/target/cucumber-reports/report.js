$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/com/features/Interests.feature");
formatter.feature({
  "name": "Arcatar Interests API\u0027s",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.scenario({
  "name": "Sample GET Request to the rest api",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@get"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the REST API endpoint \"/api/profile/interests\"",
  "keyword": "Given "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.the_REST_API_endpoint(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user sends a GET request",
  "keyword": "When "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.user_sends_a_GET_request()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response status code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.the_response_status_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Sending a POST request to create new interests",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@Post"
    },
    {
      "name": "@outofscope"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the REST API endpoint \"/api/profile/interests\"",
  "keyword": "Given "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.the_REST_API_endpoint(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user sends a Post request to the endpoint for resource create",
  "keyword": "When "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.the_user_sends_a_Post_request_to_the_endpoint_for_resource_create()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response status code should be 409",
  "keyword": "Then "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.the_response_status_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: expected:\u003c409\u003e but was:\u003c201\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat com.steps.StepDefs_InterestRequests.the_response_status_code_should_be(StepDefs_InterestRequests.java:46)\r\n\tat ✽.the response status code should be 409(file:///D:/Aracter_Project/Mahender/ArcatarTestAutomation/APIAutomation/src/test/resources/com/features/Interests.feature:15)\r\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Deleting a resource using DELETE request",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@delete"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the rest delete api \"/api/profile/gamers/\" with gamerid \"65675fcc727dc025b7a6c167\" and \"/interests/\" with interstid \"651c2243a99f392a1e4d1149\"",
  "keyword": "Given "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.the_rest_delete_api_with_gamerid_and_with_interstid(java.lang.String,java.lang.String,java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a DELETE request is sent to the endpoint for resource deletion",
  "keyword": "When "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.a_DELETE_request_is_sent_to_the_endpoint_for_resource_deletion()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response status code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "com.steps.StepDefs_InterestRequests.the_response_status_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: expected:\u003c200\u003e but was:\u003c404\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat com.steps.StepDefs_InterestRequests.the_response_status_code_should_be(StepDefs_InterestRequests.java:46)\r\n\tat ✽.the response status code should be 200(file:///D:/Aracter_Project/Mahender/ArcatarTestAutomation/APIAutomation/src/test/resources/com/features/Interests.feature:21)\r\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
});