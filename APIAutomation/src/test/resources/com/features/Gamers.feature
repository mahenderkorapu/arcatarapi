@smoke
Feature: Arcatar Gamers API's
 
  @get 
  Scenario: GET Gamers
	Given the Gamers API endpoint
	When the user sends GET request
	Then the response status code should be 200	
	
  @post @outofscope
  Scenario: Create Gamers
	Given the Gamers API endpoint
	And the "Gamers" payload
	When the user sends POST request
	Then the response status code should be 201
		
  @put
  Scenario: Update Gamers with gamerId
	Given the Gamers API endpoint with "/gamerId"	
	And the "Gamers" payload
	When the user sends PUT request
	Then the response status code should be 409
	
  @delete
  Scenario: Delete interest for the Gamer 
   	Given the Gamers API endpoint "/{gamerId}/interests/{interestId}"
   	And with gamerId "65675fcc727dc025b7a6c167" and interestId "651c2243a99f392a1e4d1149"
   	When the user sends DELETE request
    Then the response status code should be 200
    