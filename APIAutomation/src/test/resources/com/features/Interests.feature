@smoke
Feature: Arcatar Interests API's
 
  @get 
  Scenario: GET Interests
	Given the Interests API endpoint
	When the user sends GET request
	Then the response status code should be 200	
	
  @Post @outofscope
  Scenario: Create new Interests
	Given the Interests API endpoint
	And the "Interests" payload
	When the user sends POST request
	Then the response status code should be 201
		
	 @Put
  Scenario: Update Interests with interestId
	Given the Interests API endpoint with "/interestId"
	When the user sends a PUT request
	Then the response status code should be 409 
    