package com.steps;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RunWith(Cucumber.class)
@CucumberOptions(
		
        strict = true,
        features={"src/test/resources/com/features/"},
        plugin = {"pretty",
        		"html:target/cucumber-reports/",
        		"json:target/cucumber-reports/CucumberTestReport.json"
        		},
        glue = "com.steps"
        )
public class RunnerTest {
    private static Logger log = LoggerFactory.getLogger(RunnerTest.class);
    
        @BeforeClass
        public static void setUp() throws Exception {    
            log.info("Execution of Tests started");
        }
    
        @AfterClass
        public static void tearDown() throws Exception {
    
            log.info("Execution of tests finished");
        }

}
