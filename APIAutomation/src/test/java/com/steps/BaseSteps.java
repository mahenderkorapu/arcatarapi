package com.steps;

import java.io.IOException;
import java.util.logging.Logger;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import com.utilities.RestOperations;

public class BaseSteps extends RestOperations
{
    private Logger log = Logger.getLogger(this.getClass().getName());

    @Before
    public void setUp(Scenario s) throws IOException
    {
        /*
         * This method contains the code that should be executed,
         * before the scenario steps. In this setup script,
         * the method to read and set all necessary configuration data is called.
         */

        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.info("Rest API Test execution started for the test scenario: " + s.getName());
        readConfigData();
    }

    @After
    public void tearDown()
    {
        /*
         * This method contains the code that should be executed,
         * after all the scenario steps.
         */

        log.info("Rest API Test execution completed for the scenario!!");
        log.info("----------------------------------------------------------------------------");
    }
}