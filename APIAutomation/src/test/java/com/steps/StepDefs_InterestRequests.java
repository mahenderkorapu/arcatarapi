package com.steps;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefs_InterestRequests {

	private BaseSteps baseObj = null;
	private String endpoint = null;
	private JSONObject jsonPayload;
	private static Logger log = LoggerFactory.getLogger(StepDefs_InterestRequests.class);

	public StepDefs_InterestRequests(BaseSteps baseObj)
	{
		this.baseObj = baseObj;
	}

	//List of all Interest//
	@Given("the REST API endpoint {string}")
	public void the_REST_API_endpoint(String endpoint) {
		this.endpoint = endpoint;
		baseObj.setEndpoint(this.endpoint);        
	}

	@When("user sends a GET request")
	public void user_sends_a_GET_request() throws IOException {
		log.info("Submitting the GET API request for the endpoint!!");
		baseObj.restGetOperation();
		log.info("GET Request on the API is submitted!!");
	}
	@Then("the response status code should be {int}")
	public void the_response_status_code_should_be(int expectedStatusCode) {
		int responseCode = baseObj.getResponseStatusCode();
		log.info("responseCode: " + responseCode);
		Assert.assertEquals(expectedStatusCode, responseCode);
		
	}

	//Create interest method//
	@SuppressWarnings("unchecked")
	@When("the user sends a Post request to the endpoint for resource create")
	public void the_user_sends_a_Post_request_to_the_endpoint_for_resource_create() throws IOException, ParseException {
		log.info("Submitting the POST API request for the endpoint!!");  

		JSONParser parser = new JSONParser();
		FileReader reader = new FileReader("src/test/resources/com/scheme/CreateInterest.json"); 
		Object obj = parser.parse(reader);

		jsonPayload = (JSONObject) obj;
		jsonPayload.put("name", jsonPayload.get("name"));
		jsonPayload.put("description", jsonPayload.get("description"));
		jsonPayload.put("mentions", jsonPayload.get("mentions"));

		baseObj.setPayload(jsonPayload);  		
		baseObj.restPostOperation();
		log.info("POST Request on the API is submitted!!");
	}	

	//Delete Operation methods
	@Given("the rest delete api {string} with gamerid {string} and {string} with interstid {string}")
	public void the_rest_delete_api_with_gamerid_and_with_interstid(String endpoint, String gamerid, String interest, String interestid) {
		if (log != null) {
			this.endpoint = endpoint.concat(gamerid).concat(interest).concat(interestid);
			log.info("Delete Endpoint: " + this.endpoint);
		} else {
			System.out.println("Log object is null. Cannot log information.");
		}

		if (baseObj != null) {
			baseObj.setEndpoint(this.endpoint);
		} else {
			System.out.println("BaseObj is null. Cannot set endpoint.");
		}
	}

	@When("a DELETE request is sent to the endpoint for resource deletion")
	public void a_DELETE_request_is_sent_to_the_endpoint_for_resource_deletion()  throws IOException {
		log.info("Submitting the Delete API request for the endpoint!!");
		baseObj.restDeleteOperation();
		log.info("Delete Request on the API is submitted!!");
	}
}