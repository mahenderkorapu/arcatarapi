package com.utilities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.commons.validator.UrlValidator;
import org.json.simple.JSONObject;
import org.junit.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;


public class RestOperations {
	/*
	 * This class defines the operations on RESTFUL method. It also defines the methods
	 * that does return the response status code and the necessary data.
	 * The header elements are maintained in the configuration properties file,
	 * which we read under the defined path and get the values.
	 * The Host value will be passed as the runtime parameter.
	 */

	// Variables defined for loggers and necessary header elements..
	private Logger log = Logger.getLogger(this.getClass().getName());

	private Response response = null;
	private String contentType = null;
	private String endpoint = null;
	private String date[];
	public static String testEnvironment;
	public static String secretKey;
	private String payload = null;

	// Method to read the configuration file..
	public void readConfigData() throws IOException
	{
		/*
		 * This method reads the configuration data under the given path and,
		 * set the necessary header values.
		 */

		Properties prop = null;
		String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\com\\assets\\config.properties";
		log.info("Reading config file from the filepath: " + filePath);

		InputStream inStream = new FileInputStream(filePath);
		prop = new Properties();
		prop.load(inStream);

		log.info("Configuration details are getting captured!!");
		if (System.getProperty("testEnvironment").isEmpty()) {
			throw new RuntimeException(
					"Host/ Test environment is not provided. Please provide environment for the tests to run!!");
		} else {
			testEnvironment = System.getProperty("testEnvironment");
			setHost(prop.getProperty(testEnvironment + "_host_url"));
		}

		setContentType(prop.getProperty("Accept"));
		log.info("Configuration properties values are set!!");
	}

	// Method to set the value for the header element 'Content-Type'..
	private void setContentType(String contentType) {

		this.contentType = contentType;
	}

	// Method to get the value of the header element 'Content-Type'..
	public String getContentType() {

		/* This method gets the header element 'Content Type'. */
		return contentType;
	}

	// Method to set the host URI to submit the rest request..
	private void setHost(String host) {

		log.info("Host URI:  " + host);
		RestAssured.baseURI = host;
	}

	// Method to submit the GET request on the given endpoint..
	public void restGetOperation() throws IOException
	{
		/*
		 * This method takes the endpoint as a parameter and submits the get request against it,
		 * by setting the values on header elements,
		 * and finally captures the response.
		 */

		String endpoint = "";
		if(getEndpoint() != null)
			endpoint = getEndpoint();

		log.info("Endpoint : " + endpoint);
		log.info("Get HostURL:" + RestAssured.baseURI + endpoint);

		response =
				given().
				// header("Authorization", secretKey).
				accept(getContentType()).
				when().log().all().
				get(endpoint).
				then().
				extract().
				response();
		
		log.info("The response status code: " + getResponseStatusCode());
		log.info("The response body is:" + getResponseString());
		log.info("GET Request URL: " + RestAssured.baseURI + RestAssured.basePath);
		log.info("The response time: " + response.getTimeIn(TimeUnit.SECONDS) + "seconds");

	}

	public void restPostOperation() {
		/*
		 * This method takes the endpoint as a parameter and submits the POST
		 * request against it, by setting the values on header elements along
		 * with the Payload, and finally captures the response.
		 */
		String endpoint = "";
		if(getEndpoint() != null)
			endpoint = getEndpoint();

		log.info("Endpoint : "+ endpoint);
		log.info("Post HostURL:"+RestAssured.baseURI + endpoint);

		response = given()
				.accept(getContentType())
				.contentType(getContentType())
				.body(this.payload).when().log().all().post(endpoint);

		log.info("The response status code: " + getResponseStatusCode());
		log.info("The response body is:" + getResponseString());
		log.info("POST Request URL: " + RestAssured.baseURI + RestAssured.basePath);
		log.info("The response time: " + response.getTimeIn(TimeUnit.SECONDS) + "seconds");
	}

	public void restDeleteOperation() throws IOException {
		/*
		 * This method takes the endpoint as a parameter and submits the Delete
		 * request against it, by setting the values on header elements along
		 * with the Payload, and finally captures the response.
		 */
		String endpoint = "";
		if(getEndpoint() != null)
			endpoint = getEndpoint();

		log.info("Endpoint : "+ endpoint);
		log.info("Delete HostURL:"+RestAssured.baseURI + endpoint);


		response = given()
				.accept(getContentType())
				.contentType(getContentType())
				//.body(this.payload)
				.when().log().all().delete(endpoint);
		log.info("The response status code: " + getResponseStatusCode());
		log.info("The response body is:" + getResponseString());
		log.info("Delete Request URL: " + RestAssured.baseURI + RestAssured.basePath);
		log.info("The response time: " + response.getTimeIn(TimeUnit.SECONDS) + "seconds");
	}

	public void setPayload(JSONObject payload) {
		/*
		 * This method is to set the jsonBody for POST, PUT and DELETE services
		 */
		this.payload = payload.toString();
	}

	// Method to get the response status code..
	public int getResponseStatusCode()
	{
		/*
		 * This method on the submission of any REST service request,
		 * returns the response status code.
		 */

		return response.getStatusCode();
	}

	// Method to get the response type..
	public String getResponseType()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * returns the response type.
		 */

		return response.getContentType();
	}

	// Method to get the response data..
	public String getResponseString()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * returns the response as a string.
		 */

		return response.asString();
	}

	// Method to get the endpoint..
	public String getEndpoint() {
		/*
		 * This method gets the endpoint for test.
		 */

		return endpoint;
	}

	// Method to set the endpoint..
	public void setEndpoint(String endpoint) {
		/*
		 * This method sets the endpoint for test.
		 */

		this.endpoint = endpoint;
	}

	// Method to verify the response data..
	public void verifyID()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * verifies the response has the property @id
		 * and the retrieved response data is against the valid input test data given.
		 */

		Assert.assertTrue(response.body().asString().contains("id"));
	}

	// Method to verify the response data..
	public void verifyTitleAndDescription()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * verifies the response has the properties like title, description
		 * and the retrieved response data is against the valid input test data given.
		 */

		Assert.assertTrue(response.body().asString().contains("title"));
		Assert.assertTrue(response.body().asString().contains("description"));
	}

	// Method to verify the response data..
	public void verifyCreatedAtAndUpdatedAt()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * verifies the response has the properties like created_at, updated_at
		 * and the retrieved response data is against the valid input test data given.
		 */

		Assert.assertTrue(response.body().asString().contains("created_at"));
		Assert.assertTrue(response.body().asString().contains("updated_at"));
	}

	// Method to verify the fields in the response that ends with _ids are arrays..
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean verifyFieldsAsArrays()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * verifies the response fields ending with _ids are arrays.
		 */

		List<Response> stack = response.path("page.stack");
		int stackSize = stack.size();

		for(int k = 0; k < stackSize; k ++)
		{
			log.info("^^^^^^^^^^^^^ STACK [" + k + "] ^^^^^^^^^^^^^^^^^");

			HashMap<String, String> stackMap = (HashMap<String, String>) stack.get(k);
			Iterator<Map.Entry<String, String>> stackElements = stackMap.entrySet().iterator();

			while (stackElements.hasNext()) {

				Map.Entry elemPair = (Map.Entry) stackElements.next();
				String key = (String) elemPair.getKey();

				if (key.equals("data")) {

					log.info("---------------------------------------");
					List<Response> data =  (List<Response>) elemPair.getValue();
					int s1 = data.size();

					for (int i = 0; i < s1; i ++)
					{
						HashMap<String, String> subD1 = (HashMap<String, String>) data.get(i);

						Iterator it = subD1.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry pairs = (Map.Entry) it.next();
							String ids = (String) pairs.getKey();

							if (ids.contains("_ids"))	{
								log.info("*********" + pairs.getKey() + " = " + pairs.getValue());
								List<String> _ids = (List<String>) pairs.getValue();

								if(_ids.size() > 0 )
								{
									log.info("THIS IS AN ARRAY!!!!!!");
									return true;
								}
							}
						}
					}
				}
			}
		}

		log.info("No fields containing _ids are found!!");
		return true;
	}

	// Method to verify the date fields in the response contain a valid date fields..
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean verifyValidDateFormat()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * verifies the response fields created_at and updated_at has valid date values.
		 */

		List<Response> stack = response.path("page.stack");
		int stackSize = stack.size();

		for(int k = 0; k < stackSize; k ++)
		{
			log.info("^^^^^^^^^^^^^ STACK [" + k + "] ^^^^^^^^^^^^^^^^^");

			HashMap<String, String> stackMap = (HashMap<String, String>) stack.get(k);
			Iterator<Map.Entry<String, String>> stackElements = stackMap.entrySet().iterator();

			while (stackElements.hasNext()) {

				Map.Entry elemPair = (Map.Entry) stackElements.next();
				String key = (String) elemPair.getKey();

				if (key.equals("data")) {

					log.info("---------------------------------------");
					List<Response> data =  (List<Response>) elemPair.getValue();
					int s1 = data.size();

					for (int i = 0; i < s1; i ++)
					{
						HashMap<String, String> subD1 = (HashMap<String, String>) data.get(i);
						Iterator it = subD1.entrySet().iterator();

						while (it.hasNext()) {
							Map.Entry pairs = (Map.Entry) it.next();
							String dateKey = (String) pairs.getKey();

							if (dateKey.contains("created_at") || dateKey.contains("updated_at"))	{

								String response_date = (String) pairs.getValue();
								log.info("=======response_date " + response_date);
								date = response_date.split("T");

								DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
								formatter.setLenient(false);

								try {
									Date frmtdDate = formatter.parse(date[0]);
									log.info("Date is in valid format " + frmtdDate + " for the date field + '" + dateKey + "'");
									return true;
								}
								catch (Exception e) {
									// If input date is in different format or invalid.
									log.log(Level.SEVERE, "Date in the field " + dateKey + " has date value " + response_date + " is not in valid format!!");
									return false;
								}
							}
						}
					}
				}
			}
		}

		log.info("No date fields found!!");
		return true;
	}

	// Method to verify the field ending with _url in the response contain a valid url..
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean verifyValidUrl()
	{
		/*
		 * This method on the submission of REST GET service request,
		 * verifies the response fields ending with _url has a valid url.
		 */

		List<Response> stack = response.path("page.stack");
		int stackSize = stack.size();

		for(int k = 0; k < stackSize; k ++)
		{
			log.info("^^^^^^^^^^^^^ STACK [" + k + "] ^^^^^^^^^^^^^^^^^");

			HashMap<String, String> stackMap = (HashMap<String, String>) stack.get(k);
			Iterator<Map.Entry<String, String>> stackElements = stackMap.entrySet().iterator();

			while (stackElements.hasNext()) {

				Map.Entry elemPair = (Map.Entry) stackElements.next();
				String key = (String) elemPair.getKey();

				if (key.equals("data")) {

					log.info("---------------------------------------");
					List<Response> data =  (List<Response>) elemPair.getValue();
					int s1 = data.size();

					for (int i = 0; i < s1; i ++)
					{
						HashMap<String, String> subD1 = (HashMap<String, String>) data.get(i);
						Iterator it = subD1.entrySet().iterator();

						while (it.hasNext()) {
							Map.Entry pairs = (Map.Entry) it.next();
							String urlKey = (String) pairs.getKey();

							if(urlKey.contains("_url"))
							{
								String response_url = (String) pairs.getValue();
								log.info("=======response_url " + response_url);

								String[] types = {"http","https"};
								UrlValidator urlValidator = new UrlValidator(types);

								if (urlValidator.isValid(response_url)) {
									log.info("Url is valid for the field: " + urlKey);
									return true;
								} else {
									log.info("Url is not valid for the field: " + urlKey + " containing value + " + response_url);
									return false;
								}
							}
						}
					}
				}
			}
		}

		log.info("No fields with _url found in the response!!");
	
		return true;
	}
}