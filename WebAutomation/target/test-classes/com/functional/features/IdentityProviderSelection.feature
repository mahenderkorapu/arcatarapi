@smoke @regression @identity 
Feature: Select Identity providers on the ARCatar Landing Page 

Scenario: Select Google Identity 
	Given user is on Arcatar Landing page 
	When user selects the Google Identity 
	Then user navigates to Google SignIn page 

@outofscope
Scenario: Select Facebook Identity 
	Given user is on Arcatar Landing page 
	When user selects the Facebook Identity 
	Then user navigates to Facebook SignIn page 
	
Scenario: Select Apple Identity 
	Given user is on Arcatar Landing page 
	When user selects the Apple Identity 
	Then user navigates to Apple SignIn page 
	
Scenario: Select Amazon Identity 
	Given user is on Arcatar Landing page 
	When user selects the Amazon Identity 
	Then user navigates to Amazon SignIn page 
	
Scenario: Select Discord Identity 
	Given user is on Arcatar Landing page 
	When user selects the Discord Identity 
	Then user navigates to Discord SignIn page