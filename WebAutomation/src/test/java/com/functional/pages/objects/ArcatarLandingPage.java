package com.functional.pages.objects;

import org.openqa.selenium.WebDriver;

import com.functional.identifiers.ArcatarLandingPageLocators;
import com.functional.utilities.Helpers;

public class ArcatarLandingPage 
{
	WebDriver driver;
	private Helpers helper;
	private ArcatarLandingPageLocators arcLandingPageLocObj;
	
	public ArcatarLandingPage(WebDriver driver)
	{
		this.driver = driver;
		helper = new Helpers(driver);
		arcLandingPageLocObj = new ArcatarLandingPageLocators();
	}
	
	public void selectAmazonIdentity() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(arcLandingPageLocObj.loc_Btn_AmazonIdentity);
		if(elementloaded)
		{
			helper.click(arcLandingPageLocObj.loc_Btn_AmazonIdentity);				
		}
	}		
	
	public void selectGoogleIdentity() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(arcLandingPageLocObj.loc_Btn_GoogleIdentity);
		if(elementloaded)
		{
			helper.click(arcLandingPageLocObj.loc_Btn_GoogleIdentity);
		}
	}
	
	public void selectFacebookIdentity() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(arcLandingPageLocObj.loc_Btn_FacebookIdentity);
		if(elementloaded)
		{
			helper.click(arcLandingPageLocObj.loc_Btn_FacebookIdentity);
		}
	}
	
	public void selectAppleIdentity() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(arcLandingPageLocObj.loc_Btn_AppleIdentity);
		if(elementloaded)
		{
			helper.click(arcLandingPageLocObj.loc_Btn_AppleIdentity);
		}
	}
	
	public void selectDiscordIdentity() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(arcLandingPageLocObj.loc_Btn_DiscordIdentity);
		if(elementloaded)
		{
			helper.click(arcLandingPageLocObj.loc_Btn_DiscordIdentity);
		}
	}
	
	public void navigateBack()
	{
		driver.navigate().back();
	}
	
	public String getTitle()
	{
		return helper.getPageTitle();
		
	}
	
	public String getExpectedAmazonLoginTitle()
	{
		return "Amazon";
	}
	
	public String getExpectedFacebookLoginTitle()
	{
		return "Facebook";
	}
	
	public String getExpectedGoogleLoginTitle()
	{
		return "Google";
	}
	
	public String getExpectedAppleLoginTitle()
	{
		return "Apple";
	}
	
	public String getExpectedDiscordLoginTitle()
	{
		return "Discord";
	}
}