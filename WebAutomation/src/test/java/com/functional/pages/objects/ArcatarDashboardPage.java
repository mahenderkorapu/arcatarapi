package com.functional.pages.objects;

import org.openqa.selenium.WebDriver;

import com.functional.identifiers.ArcatarDashboardPageLocators;
import com.functional.utilities.Helpers;

public class ArcatarDashboardPage 
{
	WebDriver driver;
	private Helpers helper;
	private ArcatarDashboardPageLocators arcDashboardPageLocObj;
	
	public ArcatarDashboardPage(WebDriver driver)
	{
		this.driver = driver;
		helper = new Helpers(driver);
		arcDashboardPageLocObj = new ArcatarDashboardPageLocators();
	}
	
	public String getExpectedURL() throws InterruptedException
	{
		return helper.getTestURLs().getProperty("devURL") + "account";
	}		
}