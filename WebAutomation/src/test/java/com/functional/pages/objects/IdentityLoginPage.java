package com.functional.pages.objects;

import org.openqa.selenium.WebDriver;

import com.functional.identifiers.IdentityLoginPageLocators;
import com.functional.utilities.Helpers;

public class IdentityLoginPage 
{
	WebDriver driver;
	private Helpers helper;
	private IdentityLoginPageLocators loginPageLocObj;
	
	public IdentityLoginPage(WebDriver driver)
	{
		this.driver = driver;
		helper = new Helpers(driver);
		loginPageLocObj = new IdentityLoginPageLocators();
	}	
	
	public void enterUserName() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(loginPageLocObj.loc_Amazon_Txt_Username);
		if(elementloaded)
		{
			helper.sendKeys(loginPageLocObj.loc_Amazon_Txt_Username,helper.getTestData().getProperty("AmazonUsername"));
		}
	}
	
	public void enterPassword() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(loginPageLocObj.loc_Amazon_Txt_Password);
		if(elementloaded)
		{
			helper.sendKeys(loginPageLocObj.loc_Amazon_Txt_Password,helper.getTestData().getProperty("AmazonPassword"));			
			helper.click(loginPageLocObj.loc_Amazon_Btn_Submit);
		}
	}
	
	public void submit() throws InterruptedException
	{
		boolean elementloaded = helper.pollElement(loginPageLocObj.loc_Amazon_Btn_Submit);
		if(elementloaded)
		{
			helper.click(loginPageLocObj.loc_Amazon_Btn_Submit);
		}
	}
	
	public String getExpectedAmazonUrl()
	{
		return "amazon.com";
	}	
	
	public String getExpectedArcLoginTitle()
	{
		return "ARCatar | Login";
	}
}