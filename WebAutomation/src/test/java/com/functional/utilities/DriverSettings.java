package com.functional.utilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverSettings 
{
	private WebDriver driver;
	private Logger log = Logger.getLogger(DriverSettings.class.getName());
	
	public WebDriver getDriver() {
		return driver;
	}

	public WebDriver initializeDriver()
	{
		log.info("Running locally.... WebDriver instance initialized!!");
		
		String browser = System.getProperty("test_browser");
		switch(browser) {
		case "chrome":
			chromeInitilize();
			break;
		case "IE":
			IEInitilize();
			break;
		case "firefox":
			firefoxInitilize();
			break;
		default:
			chromeInitilize();
		}
		return driver;
	}
	
	public WebDriver chromeInitilize()
	{
		try {
			WebDriverManager.chromedriver().setup();

			ChromeOptions options = new ChromeOptions();
			boolean isHeadless = Boolean.parseBoolean(System.getProperty("testbrowser.headless"));
			if(isHeadless)
				options.addArguments("--headless");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-setuid-sandbox");
			options.addArguments("--start-maximized");  	        // Maximize the browser window
			options.addArguments("--disable-extensions"); 		    // Disable browser extensions
			options.addArguments("--incognito");        		    // Launch WebDriver with incognito mode
			options.addArguments("--ignore-certificate-errors");    // Ignore certificate error for browser
			options.addArguments("--disable-application-cache");    // Disable application cache

			driver = new ChromeDriver(options);
			driver.manage().window().maximize();

		} catch (Exception e) {
			log.info(
					"Tests not executed because of the incorrect 'Chrome' driver settings. Please check before run - "
							+ e.getMessage());
		}
		return driver;
	}
	
	public WebDriver firefoxInitilize()
	{
		try {
			WebDriverManager.firefoxdriver().setup();

			FirefoxOptions options = new FirefoxOptions();
			boolean isHeadless = Boolean.parseBoolean(System.getProperty("testbrowser.headless"));
			if(isHeadless)
				options.addArguments("--headless");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-setuid-sandbox");
			options.addArguments("--start-maximized");  	        // Maximize the browser window
			options.addArguments("--disable-extensions"); 		    // Disable browser extensions
			options.addArguments("--incognito");        		    // Launch WebDriver with incognito mode
			options.addArguments("--ignore-certificate-errors");    // Ignore certificate error for browser
			options.addArguments("--disable-application-cache");    // Disable application cache

			driver=new FirefoxDriver(options);
			driver.manage().window().maximize();
		} catch (Exception e) {
			log.info(
					"Tests not executed because of the incorrect 'FireFox' driver settings. Please check before run - "
							+ e.getMessage());
		}
		return driver;
	}
	
	public WebDriver IEInitilize()
	{
		try {
			WebDriverManager.iedriver().setup();

			driver=new InternetExplorerDriver();
			driver.manage().window().maximize();
		} catch (Exception e) {
			log.info(
					"Tests not executed because of the incorrect 'IE' driver settings. Please check before run - "
							+ e.getMessage());
		}
		return driver;
	}
	
	public Properties readConfig() {
		log.info("Reading Configuration file..........");
		Properties prop = new Properties();

		try {
			URL res = getClass().getClassLoader().getResource("com/functional/assets/config.properties");
			File f = new File(res.getFile());

			log.info("Loading Configuration file.........." + f.getAbsolutePath());
			prop.load(res.openStream());
			log.info("Properties file loaded..........");
		} catch (IOException e) {

			log.log(Level.SEVERE, "Error while loading the configuration values into the test environment parameters!! "
					+ e.getMessage());
		} catch (NullPointerException e) {

			log.log(Level.SEVERE, "Error while finding the configuration file!! " + e.getMessage());
		}
		return prop;
	}
	
	public void closeDriver() 
	{
		driver.close();
	}
}