package com.functional.utilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.NoSuchElementException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Helpers {
	
	private WebDriver driver;
	private Actions action;
	int time = 40000;
	
	private Logger log = Logger.getLogger(Helpers.class.getName());
	
	public Helpers(WebDriver driver)
	{
		this.driver = driver;
		action = new Actions(driver);
	}

	public void getArcatarURL() throws InterruptedException
	{
		Properties prop = new Properties();
		prop = getTestURLs();
		String testenvironment = System.getProperty("test_environment");
		
		switch(testenvironment) {
		case "dev":
			getURL(prop.getProperty("devURL"));
			break;
		case "stage":
			getURL(prop.getProperty("stgURL"));
			break;
		case "prod":
			getURL(prop.getProperty("prodURL"));
			break;
		default:
			log.info("Please provide valid Test Environment!!! By default, using dev. environment: " + testenvironment);
			getURL(prop.getProperty("devURL"));
		}
	}
	
	public Properties getTestURLs()
	{
		Properties prop = new Properties();

		try {	
			URL res = getClass().getClassLoader().getResource("com/functional/assets/testUrl.properties");
			File f = new File(res.getFile());
			
			log.info("Loading Test URLs file.........." + f.getAbsolutePath());
			prop.load(res.openStream());
			log.info("Test URLs file loaded..........");
		} catch (IOException e) {
			log.log(Level.SEVERE,
					"Error while loading the test URLs values into the test parameters!! " + e.getMessage());			
		}
		return prop;
	}
	
	public void getURL(String URL) throws InterruptedException
	{
		driver.manage().deleteAllCookies();
		driver.navigate().refresh();
		waitForMilliSeconds(2000);
		driver.navigate().to(URL);
	}
	
	// Method to read the test data from the properties file..
	public Properties getTestData() {
		Properties prop = new Properties();

		try {
			URL res = getClass().getClassLoader().getResource("com/functional/assets/testdata.properties");
			File f = new File(res.getFile());

			log.info("Loading Test Data file.........." + f.getAbsolutePath());
			prop.load(res.openStream());
			log.info("Test Data file loaded..........");
		} catch (IOException e) {

			log.log(Level.SEVERE,
					"Error while loading the test data values into the test parameters!! " + e.getMessage());
		}
		return prop;
	}	
	
	public void moveToElement_Click(By locator1, By locator2) throws InterruptedException
	{
		moveToElement(locator2);
		WebElement elem = driver.findElement(locator1);
		elem.click();
	}
	
	public void moveToElement(By locator)
	{
		WebElement element = driver.findElement(locator);
		action.moveToElement(element).build().perform();
	}	
	
	public void click(By locator) throws InterruptedException
	{
		WebElement elem = driver.findElement(locator);
		elem.click();
	}
	
	public boolean pollElement(By locator) {
		long startTime;
		long currentTime = startTime = System.currentTimeMillis();
		int attempts = 1;
		while (currentTime - startTime <= time) {
			try {
				WebElement elem = driver.findElement(locator);
				if (elem != null)
					return true;
			} catch (StaleElementReferenceException e) {
				currentTime = System.currentTimeMillis();
				++attempts;
				continue;
			} catch (Exception e) {
				currentTime = System.currentTimeMillis();
				++attempts;
				continue;
			}
			throw new NoSuchElementException(
					String.format("Element not found: \"%s\" after %s seconds of trying and %s attempts", locator,
							((currentTime - startTime) / 1000), attempts));
		}
		return false;
	}
	
	public void Waitfor(long time)
	{
		long startTime;
		long currentTime = startTime = System.currentTimeMillis();
		while (currentTime - startTime <= time)
		{
			currentTime = System.currentTimeMillis();
		}
	}
	
	public void waitForMilliSeconds(int milliSec) throws InterruptedException
	{
		Thread.sleep(milliSec);
	}
	
	public void sendKeys(By locator, String content) throws InterruptedException
	{
		driver.findElement(locator).sendKeys(content);
	}

	public void clickEnter(By locator)
	{
		driver.findElement(locator).sendKeys(Keys.ENTER);
	}
	
	// Method to get the title of the current web page..
	public String getPageTitle() 
	{
		String value = driver.getTitle();
		return value;
	}
	
	// Method to get the current URL of the web page..
	public String getCurrentURL()
	{
		String url = driver.getCurrentUrl();
		return url;
	}
}