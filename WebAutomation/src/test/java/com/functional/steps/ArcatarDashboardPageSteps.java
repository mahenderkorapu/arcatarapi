package com.functional.steps;

import java.util.logging.Logger;
import org.testng.Assert;
import io.cucumber.java.en.Then;

public class ArcatarDashboardPageSteps {

	private BaseClass baseObj;
	private Logger log = Logger.getLogger(ArcatarDashboardPageSteps.class.getName());

	public ArcatarDashboardPageSteps(BaseClass baseObj) {
		this.setBaseObj(baseObj);
	}

	public BaseClass getBaseObj() {
		return baseObj;
	}

	private void setBaseObj(BaseClass baseObj) {
		this.baseObj = baseObj;
	}
	
	@Then("user navigates to Arcatar Dashboard page")
    public void user_navigates_to_arcatar_dashboard_page() throws InterruptedException {		
    	log.info(getBaseObj().getHelper().getPageTitle() + " Dashboard Page");
        Assert.assertEquals(getBaseObj().getArcatarDashboardPage().getExpectedURL(), getBaseObj().getHelper().getCurrentURL());
     }	
}