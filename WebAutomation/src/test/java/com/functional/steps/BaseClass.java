package com.functional.steps;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.functional.pages.objects.ArcatarDashboardPage;
import com.functional.pages.objects.ArcatarLandingPage;
import com.functional.pages.objects.IdentityLoginPage;
import com.functional.utilities.DriverSettings;
import com.functional.utilities.Helpers;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class BaseClass extends DriverSettings{
	
	private WebDriver driver;
	private Helpers helper;
	private IdentityLoginPage loginPageObj;
	private ArcatarLandingPage arcatarLandingPageObj;
	private ArcatarDashboardPage arcatarDashboardPageObj;
	private Logger log = Logger.getLogger(BaseClass.class.getName());
	
	@Before
	public void setUp(Scenario scenario)
	{ 
		readConfig();		
		initializeDriver();		
		log.info("-------------------------------------------------------------------------");
		log.info(".......................Browser has started sucessfully....................");
		log.info("......." + scenario.getName() + " Test Execution has started ......");
		log.info("-------------------------------------------------------------------------");
		driver = getDriver();
		initializeClasses();
	}
	
	public void initializeClasses()
	{
		helper = new Helpers(driver);
		loginPageObj = new IdentityLoginPage(driver);
		arcatarLandingPageObj = new ArcatarLandingPage(driver);
		arcatarDashboardPageObj = new ArcatarDashboardPage(driver);
	}
	
	public Helpers getHelper()
	{
		return helper;
	}
	
	public IdentityLoginPage getLoginPage()
	{
		return loginPageObj;
	}
	
	public ArcatarLandingPage getArcatarLandingPage()
	{
		return arcatarLandingPageObj;
	}
	
	public ArcatarDashboardPage getArcatarDashboardPage()
	{
		return arcatarDashboardPageObj;
	}
	
	public void takeScreenshotOnFailure(Scenario scenario) {

		if (scenario.isFailed()) {
			log.info("-------------------------------------------------------------------");
			log.info("Test Failed for the scenario: " + scenario.getName());
			log.info("-------------------------------------------------------------------");
			File f = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(f, new File("target/FailedScreenshots/" + scenario.getName() + "/Img.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			log.info("-------------------------------------------------------------------");
			log.info("Test Passed for the scenario: " + scenario.getName());
			log.info("-------------------------------------------------------------------");
		}
	}
	
	@After
	public void teardown(Scenario scenario) throws IOException {

		takeScreenshotOnFailure(scenario);
		closeDriver();
		log.info("-------------------------------------------------------------------");
		log.info("Test Execution completed for the scenario successfully: " + scenario.getName());
		log.info("-------------------------------------------------------------------");
	}
}