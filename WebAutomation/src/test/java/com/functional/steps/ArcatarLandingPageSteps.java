package com.functional.steps;

import java.util.logging.Logger;
import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ArcatarLandingPageSteps {

	private BaseClass baseObj;
	private Logger log = Logger.getLogger(ArcatarLandingPageSteps.class.getName());

	public ArcatarLandingPageSteps(BaseClass baseObj) {
		this.setBaseObj(baseObj);
	}

	public BaseClass getBaseObj() {
		return baseObj;
	}

	private void setBaseObj(BaseClass baseObj) {
		this.baseObj = baseObj;
	}

	@Given("user is on Arcatar Landing page")
	public void user_is_on_arcatar_landing_page() throws InterruptedException {
		getBaseObj().getHelper().getArcatarURL();
		log.info("Launching Arcatar Application ......");
		Assert.assertEquals("ARCatar | Login", getBaseObj().getArcatarLandingPage().getTitle()); 	     
	}

	@When("user selects the Google Identity")
	public void user_selects_the_Google_Identity() throws InterruptedException {
		getBaseObj().getArcatarLandingPage().selectGoogleIdentity();
		log.info("User selected Google Identity ......");
	}

	@When("user selects the Facebook Identity")
	public void user_selects_the_Facebook_Identity() throws InterruptedException {
		getBaseObj().getArcatarLandingPage().selectFacebookIdentity();
		log.info("User selected Facebook Identity ......");
	}
	
	@When("user selects the Apple Identity")
	public void user_selects_the_Apple_Identity() throws InterruptedException {
		getBaseObj().getArcatarLandingPage().selectAppleIdentity();
		log.info("User selected Apple Identity ......");
	}

	@When("user selects the Amazon Identity")
	public void user_selects_the_Amazon_Identity() throws InterruptedException {
		getBaseObj().getArcatarLandingPage().selectAmazonIdentity();
		log.info("User selected Amazon Identity ......");
	}

	@When("user selects the Discord Identity")
	public void user_selects_the_Discord_Identity() throws InterruptedException {
		getBaseObj().getArcatarLandingPage().selectDiscordIdentity();
		log.info("User selected Discord Identity ......");
	}

	@Then("user navigates to Google SignIn page")
	public void user_navigates_to_Google_SignIn_page() throws InterruptedException {	
		Assert.assertTrue(getBaseObj().getHelper().getPageTitle().contains(getBaseObj().getArcatarLandingPage().getExpectedGoogleLoginTitle()));   
	}

	@Then("user navigates to Facebook SignIn page")
	public void user_navigates_to_Facebook_SignIn_page() throws InterruptedException {		
		Assert.assertTrue(getBaseObj().getHelper().getPageTitle().contains(getBaseObj().getArcatarLandingPage().getExpectedFacebookLoginTitle()));    
	}
	
	@Then("user navigates to Apple SignIn page")
	public void user_navigates_to_Apple_SignIn_page() throws InterruptedException {
		Assert.assertTrue(getBaseObj().getHelper().getPageTitle().contains(getBaseObj().getArcatarLandingPage().getExpectedAppleLoginTitle()));
	}

	@Then("user navigates to Amazon SignIn page")
	public void user_navigates_to_Amazon_SignIn_page() throws InterruptedException {
		Assert.assertTrue(getBaseObj().getHelper().getPageTitle().contains(getBaseObj().getArcatarLandingPage().getExpectedAmazonLoginTitle()));
	}

	@Then("user navigates to Discord SignIn page")
	public void user_navigates_to_Discord_SignIn_page() throws InterruptedException {
		Assert.assertTrue(getBaseObj().getHelper().getPageTitle().contains(getBaseObj().getArcatarLandingPage().getExpectedDiscordLoginTitle()));
	}
}