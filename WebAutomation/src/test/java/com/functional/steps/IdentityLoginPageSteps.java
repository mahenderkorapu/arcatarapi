package com.functional.steps;

import java.util.logging.Logger;
import io.cucumber.java.en.And;
 
public class IdentityLoginPageSteps {
    
	private BaseClass baseObj;
	private Logger log = Logger.getLogger(IdentityLoginPageSteps.class.getName());

	public IdentityLoginPageSteps(BaseClass baseObj) {
		this.setBaseObj(baseObj);
	}

	public BaseClass getBaseObj() {
		return baseObj;
	}

	public void setBaseObj(BaseClass baseObj) {
		this.baseObj = baseObj;
	}
	
    @And("logs in with valid Amazon credentials")
    public void logs_in_with_valid_Amazon_credentials() throws InterruptedException {
    	getBaseObj().getLoginPage().enterUserName();
    	getBaseObj().getLoginPage().enterPassword();
    	getBaseObj().getLoginPage().submit();
    	log.info("User entered valid Amazon credentials and submitted ......");
    }    
}