package com.functional.steps;

import org.testng.annotations.DataProvider;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions( features = {"src/test/resources/com/functional/features"}, 
	glue = {"com.functional.steps"},
	plugin = {"html:target/cucumber-reports/cucumber-report.html",
		"json:target/cucumber-reports/CucumberTestReport.json"
	}
)

public class RunnerClass extends AbstractTestNGCucumberTests {

	@Override
	@DataProvider(parallel = true)
	public Object[][] scenarios() {
		return super.scenarios();
	}
}