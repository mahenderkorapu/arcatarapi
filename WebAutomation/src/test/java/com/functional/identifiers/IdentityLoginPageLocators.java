package com.functional.identifiers;

import org.openqa.selenium.By;

public class IdentityLoginPageLocators {
	
	// Amazon Identity Login Page Locators 
	public final By loc_Amazon_Txt_Username = By.xpath("//input[@id='ap_email']");
	public final By loc_Amazon_Txt_Password = By.xpath("//input[@id='ap_password']");
	public final By loc_Amazon_Btn_Submit = By.xpath("//input[@id='signInSubmit']");
}