package com.functional.identifiers;

import org.openqa.selenium.By;

public class ArcatarLandingPageLocators {

	public final By loc_Btn_GoogleIdentity = By.xpath("//button[@class='cf8ab1d76 ca5439885 c90865442']");
	public final By loc_Btn_AmazonIdentity = By.xpath("//button[@class= 'cf8ab1d76 ca5439885 c3d09e324']");	
	public final By loc_Btn_AppleIdentity = By.xpath("//button[@class='cf8ab1d76 ca5439885 cc830f88b']");
	public final By loc_Btn_DiscordIdentity = By.xpath("//button[@class='cf8ab1d76 ca5439885 _social-button-oauth2']");
	public final By loc_Btn_FacebookIdentity = By.xpath("//span[normalize-space()='Facebook']");
}