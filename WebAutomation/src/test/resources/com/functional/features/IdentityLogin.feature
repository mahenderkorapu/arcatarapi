@smoke @regression @login
Feature: Login to ARCatar Platform with different Identities 

@amazon-login
Scenario: Login with valid Amazon Identity account 
	Given user is on Arcatar Landing page 
	When user selects the Amazon Identity
	And user navigates to Amazon SignIn page 
	And logs in with valid Amazon credentials 
	Then user navigates to Arcatar Dashboard page